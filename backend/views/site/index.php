<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Управление кафе</h1>

        <p class="lead">Вы можете добавлять блюда и ингредиенты</p>

        <p><a class="btn btn-lg btn-success" href="/dish">Блюда</a> <a class="btn btn-lg btn-success" href="/ingredient">Ингредиенты</a></p>
    </div>

</div>
