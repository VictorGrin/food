<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Dish */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Блюда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dish-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить это блюдо?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'active',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

    <h2>Список ингредиентов, входящих в состав</h2>
    <ul>
    <?php
        foreach ($model->allIngredients as $ingredient) {
            $li_class = $ingredient->active == 0 ? ' class="text-danger"' : '';
            echo '<li'.$li_class.'>'.Html::encode($ingredient->name).' '.
                    Html::a('[удалить из блюда]', ['view', 'id' => $model->id, 'ingredient_id' => $ingredient->id]).'</li>';
        }
    ?>
    </ul>
    <hr />
    
    <div class="ingredient-in-dish-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($inInDishModel, 'ingredient_id')
            ->dropDownList(
                    ArrayHelper::map($ingredientsList, 'id', 'name')) ?>    

    <div class="form-group">
        <?= Html::submitButton('Добавить ингредиент в состав блюда "'.$model->name.'"', ['class' => 'btn btn-success']) ?> <?= Html::a('Добавить новый ингредиент в список', ['/ingredient/create'], ['class' => 'btn btn-muted']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>

</div>
