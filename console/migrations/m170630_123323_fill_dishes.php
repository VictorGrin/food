<?php

use yii\db\Migration;

class m170630_123323_fill_dishes extends Migration
{
    public function safeUp()
    {
        // 1
        $this->insert('dish', [
            'name' => 'Яичница', 'created_at' => time(), 'updated_at' => time(),
        ]);
        // 2
        $this->insert('dish', [
            'name' => 'Яичница с беконом', 'created_at' => time(), 'updated_at' => time(),
        ]);
        // 3
        $this->insert('dish', [
            'name' => 'Яичница с беконом и луком', 'created_at' => time(), 'updated_at' => time(),
        ]);
        // 4
        $this->insert('dish', [
            'name' => 'Пицца с грибами', 'created_at' => time(), 'updated_at' => time(),
        ]);
        // 5
        $this->insert('dish', [
            'name' => 'Пицца с колбасой и луком', 'created_at' => time(), 'updated_at' => time(),
        ]);
        // 6
        $this->insert('dish', [
            'name' => 'Чесночная пицца', 'created_at' => time(), 'updated_at' => time(),
        ]);
        // 7
        $this->insert('dish', [
            'name' => 'Овощной салат', 'created_at' => time(), 'updated_at' => time(),
        ]);
        // 8
        $this->insert('dish', [
            'name' => 'Омлет', 'created_at' => time(), 'updated_at' => time(),
        ]);
        
        $this->insert('ingredient', [ // 1
            'name' => 'Яйца', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 2
            'name' => 'Бекон', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 3
            'name' => 'Лук', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 4
            'name' => 'Чеснок', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 5
            'name' => 'Тесто', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 6
            'name' => 'Грибы', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 7
            'name' => 'Колбаса', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 8
            'name' => 'Огурцы', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 9
            'name' => 'Помидоры', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 10
            'name' => 'Соль', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 11
            'name' => 'Перец', 'created_at' => time(), 'updated_at' => time(),
        ]);
        $this->insert('ingredient', [ // 12
            'name' => 'Подсолнечное масло', 'created_at' => time(), 'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->delete('ingredient_in_dish', '1');
        $this->delete('dish', '1');
        $this->delete('ingredient', '1');
    }
}