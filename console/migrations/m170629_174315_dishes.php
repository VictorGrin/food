<?php

use yii\db\Migration;

class m170629_174315_dishes extends Migration
{
    public function safeUp()
    {
        // dish
        $this->createTable('{{%dish}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull()->comment('Наименование'),
            'active' => $this->boolean()->null()->defaultValue(1),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], null);
        
        // ingredient
        $this->createTable('{{%ingredient}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull()->comment('Наименование'),
            'active' => $this->boolean()->null()->defaultValue(1),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], null);
        
        // ingredient_in_dish
        $this->createTable('{{%ingredient_in_dish}}', [
            'id' => $this->primaryKey(),
            'dish_id' => $this->integer(11)->notNull(),
            'ingredient_id' => $this->integer(11)->notNull(),
        ], null);
        
        $this->createIndex(
            'idx-dish_ingredients',
            'ingredient_in_dish',
            ['dish_id', 'ingredient_id'],
            true
        );
        
        $this->createIndex(
            'idx-dish_id',
            'ingredient_in_dish',
            'dish_id'
        );
        
        $this->createIndex(
            'idx-ingredient_id',
            'ingredient_in_dish',
            'ingredient_id'
        );
        
        $this->addForeignKey(
            'fk_ingredient_in_dish-idx-dish_id',
            'ingredient_in_dish',
            'dish_id',
            'dish',
            'id',
            'CASCADE',
            'CASCADE'    
        );
        $this->addForeignKey(
            'fk_ingredient_in_dish-idx-ingredient_id',
            'ingredient_in_dish',
            'ingredient_id',
            'ingredient',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_ingredient_in_dish-idx-dish_id',
            'ingredient_in_dish'
        );
        $this->dropIndex(
            'idx-dish_id',
            'ingredient_in_dish'
        );

        $this->dropForeignKey(
            'fk_ingredient_in_dish-idx-ingredient_id',
            'ingredient_in_dish'
        );
        $this->dropIndex(
            'idx-ingredient_id',
            'ingredient_in_dish'
        );
        
        $this->dropTable('{{%ingredient_in_dish}}');
        
        $this->dropTable('{{%dish}}');
        $this->dropTable('{{%ingredient}}');
    }
}
