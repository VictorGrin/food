<?php

namespace frontend\modules\recipe\controllers;

use Yii;
use yii\web\Controller;
use common\models\Dish;
use common\models\DishSearch;
use common\models\Ingredient;
use common\models\IngredientSearch;

class DefaultController extends Controller
{
    /**
     * Список и поиск блюд
     * @return mixed
     */
    public function actionIndex()
    {
        $ingredientsList = Ingredient::find()->where(['active' => 1])->orderBy(['name' => SORT_ASC])->all();
        $searchModel = new DishSearch();
        $dataProvider = $searchModel->userSearch(Yii::$app->request->queryParams);

        return $this->render('index', [
            'ingredientsList' => $ingredientsList,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
