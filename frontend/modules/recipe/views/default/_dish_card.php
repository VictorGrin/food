<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;

$modelIngredients = ArrayHelper::map($model->ingredients, 'id', 'name');
$list = '<ul>';
    foreach ($model->allIngredients as $ingredient) {
        $li_class = array_search($ingredient->name, $modelIngredients) ? ' class="text-success"' : '';
        $list .= '<li'.$li_class.'>'.Html::encode($ingredient->name).'</li>';
}
$list .= '</ul>';
?>
<div class="item-card">
    <h3><?= Html::encode($model->name) ?></h3>
    <?= $list ?>
    <br />Всего ингредиентов: <?= $model->inAmount ?>
</div>