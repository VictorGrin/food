<?php

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Поиск блюд по ингредиентам';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="dish-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_search', ['model' => $searchModel, 'ingredientList' => $ingredientsList]) ?>
    <hr />
    
    <h2>Блюда</h2>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item col-lg-3'],
        'itemView' => '_dish_card',
    ]) ?>
</div>
