<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\DishSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dish-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <p>Укажите не менее двух и не более 5-ти ингредиентов.</p>
    <?php echo '<h3>Выбрано <span id="selectedIngredients">'.count($model->search_ingredients).'</span> из 5 возможных ингредиентов</h3>'; ?>
    <?php // echo $form->field($model, 'name'); ?>

    <?php
    // $model->addError('ingredients', " no asset found return error message to input corect asset number or create asset in navision and run php script to update mysql  ");
    echo $form->field($model, 'search_ingredients')
            ->checkboxList(
                    ArrayHelper::map($ingredientList, 'id', 'name'),
            ['multiple' => true, 'class' => 'ingredient']);
    ?>
    
    <?= $form->errorSummary($model); ?>
    
    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Отменить', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Сбросить', ['/recipe'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs("
var limit = 5;
$('form input:checkbox').on('change', function(evt) {
    if ($('input:checkbox:checked').length > limit) {
        this.checked = false;
    } else {
        $('#selectedIngredients').html($('input:checkbox:checked').length);
    }
});
", yii\web\View::POS_END);
?>