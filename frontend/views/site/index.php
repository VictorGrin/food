<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Кафе по рецептам</h1>

        <p class="lead">Выбери себе блюдо с тем, что тебе нравится.</p>
        <h2>Всего в базе данных</h2>
        <p class="lead">Блюд: <?= $dishes ?> Ингредиентов: <?= $ingredients ?></p>

        <p><a class="btn btn-lg btn-success" href="/recipe">Начать поиск блюд</a></p>
    </div>

</div>
