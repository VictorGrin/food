<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "dish".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property IngredientInDish[] $ingredientInDishes
 * @property Ingredient[] $ingredients
 */
class Dish extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dish';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'active'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['inAmount'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название блюда',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Дата обновления',
            'active' => 'Статус',
            'inAmount' => 'Количество ингредиентов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientInDishes()
    {
        return $this->hasMany(IngredientInDish::className(), ['dish_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id' => 'ingredient_id'])->via('ingredientInDishes');
    }
    
    /*
     * Найдём отключенные ингредиенты
     */
    public function getDeactivatedIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id' => 'ingredient_id'])->via('ingredientInDishes')->where(['=', 'active', 0])->count();
    }
    
    public function  getAllIngredients() {
        return $this->hasMany(Ingredient::className(), ['id' => 'ingredient_id'])->viaTable('ingredient_in_dish', ['dish_id' => 'id'])->orderBy(['name' => SORT_ASC]);
    }
    
    // Общее количество ингредиентов в блюде
    public function getInAmount() {
        return count($this->ingredientInDishes);
    }

    public function checkStatus() {
        if ($this->deactivatedIngredients >= 1) {
            $this->active = 0;
        } else {
            $this->active = 1;
        }
        $this->save();
    }
}
