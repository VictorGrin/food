<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Dish;

/**
 * DishSearch represents the model behind the search form about `common\models\Dish`.
 */
class DishSearch extends Dish
{
    public $search_ingredients = [];
    public $ingredientCount;
    public $ingredientMatches = 0;
    public $active = 1;
    public $inAmount;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'ingredientMatches', 'ingredientCount', 'active'], 'integer'],
            [['name', 'search_ingredients', 'inAmount'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dish::find();
        
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        
        return $dataProvider;
    }
    
    /* 
     * Search for frontend
     * 
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function userSearch($params)
    {
        $query = Dish::find()->joinWith('ingredientInDishes')->select(['dish.*', 'COUNT(ingredient_in_dish.ingredient_id) AS ingredientCount']);
        $query->groupBy(['dish.id']);

        $this->load($params);
        
        //Посчитаем количество указанных ингредиентов
        if (is_array($this->search_ingredients)) {
            $selectedIngredients = count($this->search_ingredients);
        } else {
            $selectedIngredients = 0;
        }
  
        $query->joinWith(['ingredients' => function ($subquery) {
            $subquery->where(['ingredient.id' => $this->search_ingredients]);
        }]);
        
        // Если было выбрано менее 2-х ингредиентов, то добавим условно невыполнимое условие
        
        if ($selectedIngredients < 2) {
            $query->having('ingredientCount >= 100');
        } else {
            // Теперь поищем блюда с полным совпадением ингредиентов
            $query->having('ingredientCount = '.$selectedIngredients);

            $this->ingredientMatches = $query->count();
             
            // Если не найдено полных совпадений по ингредиентам, то поищем остальные и отсортируем их по количеству совпавших ингредиентов
            if ($selectedIngredients <= 5 && $this->ingredientMatches == 0) {
                $query->orderBy(['ingredientCount' => SORT_DESC])->having('ingredientCount >= 2');
            }
        }
        
      
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['dish.active' => 1]);
        $query->andFilterWhere([
            'dish.id' => $this->id,
            'dish.created_at' => $this->created_at,
            'dish.updated_at' => $this->updated_at,
        ]);

        // $query->andFilterWhere(['like', 'dish.name', $this->name]);
        
        return $dataProvider;
    }
    
    public function attributeLabels()
    {
        return [
            'name' => 'Название блюда',
            'search_ingredients' => 'Ингредиенты',
        ];
    }
}
